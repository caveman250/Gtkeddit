#include "Message.h"
#include "IconCache.h"

namespace api
{
    Message::Message(Json::Value jsonValue, const std::string& kind)
        : m_Kind(kind)
    {
        UpdateDataFromJson(jsonValue);
    }

    void Message::UpdateDataFromJson(Json::Value jsonValue)
    {
        m_FirstMessage = jsonValue["first_message"].asUInt();
        m_FirstMessageName = jsonValue["first_message_name"].asString();
        m_Subreddit = jsonValue["subreddit"].asString();
        m_Replies = jsonValue["replies"].asString();
        m_AuthorFullname = jsonValue["author_fullname"].asString();
        m_ID = jsonValue["id"].asString();
        m_Subject = jsonValue["subject"].asString();
        m_Score = jsonValue["score"].asInt();
        m_Author = jsonValue["author"].asString();
        m_NumComments = jsonValue["num_comments"].asUInt();
        m_ParentID = jsonValue["parent_id"].asString();
        m_SubredditNamePrefixed = jsonValue["subreddit_name_prefixed"].asString();
        m_New = jsonValue["new"].asBool();
        m_Type = jsonValue["type"].asString();
        m_Body = jsonValue["body"].asString();
        m_LinkTitle = jsonValue["link_title"].asString();
        m_Dest = jsonValue["dest"].asString();
        m_WasComment = jsonValue["was_comment"].asBool();
        m_BodyHtml = jsonValue["body_html"].asString();
        m_Name = jsonValue["name"].asString();
        m_Created = jsonValue["created"].asUInt();
        m_CreatedUTC = jsonValue["created_utc"].asUInt();
        m_Context = jsonValue["context"].asString();
        m_IsModerator = jsonValue["distinguished"].asString() == "moderator";
    }
}