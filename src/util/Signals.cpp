#include "Signals.h"

namespace util
{
    static Signals* s_Instance = nullptr;

    Signals* Signals::Get()
    {
        if (!s_Instance)
        {
            s_Instance = new Signals();
        }

        return s_Instance;
    }
}