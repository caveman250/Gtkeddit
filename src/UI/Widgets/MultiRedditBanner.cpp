#include <AppSettings.h>
#include <util/Signals.h>
#include <util/HtmlParser.h>
#include "MultiRedditBanner.h"
#include "PictureWidget.h"
#include "API/MultiReddit.h"

namespace ui
{
    MultiRedditBanner::~MultiRedditBanner()
    {
        if (m_RoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_RoundedCornersSettingChangedHandler);
        }
    }

    Gtk::Box* MultiRedditBanner::CreateUI(Gtk::Box* parent, const api::MultiRedditRef& multi)
    {
        auto bannerBuilder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/multireddit.ui");

        Gtk::Box* banner = bannerBuilder->get_widget<Gtk::Box>("MultiredditPanel");
        bool rounded = AppSettings::Get()->GetBool("rounded_corners", false);
        util::Helpers::ApplyCardStyle(banner, rounded);
        m_RoundedCornersSettingChangedHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([banner](bool roundedCorners)
        {
            util::Helpers::ApplyCardStyle(banner, roundedCorners);
        });

        parent->append(*banner);

        Gtk::Box* iconBox = bannerBuilder->get_widget<Gtk::Box>("MultiIconBox");
        util::ImageData imageData;
        imageData.m_Url = multi->GetIconUrl();
        imageData.m_Width = 96;
        imageData.m_Height = 96;
        PictureWidget* pictureWidget = new PictureWidget(imageData.m_Width, imageData.m_Height, false, false);
        pictureWidget->set_halign(Gtk::Align::END);
        pictureWidget->SetImageData(imageData);
        iconBox->append(*pictureWidget);

        m_NameLabel = bannerBuilder->get_widget<Gtk::Label>("NameLabel");
        m_NameLabel->set_text(multi->GetDisplayName());

        m_DescBox = bannerBuilder->get_widget<Gtk::Box>("DescBox");
        util::HtmlParser htmlParser;
        htmlParser.ParseHtml(multi->GetDescriptionHtml(), m_DescBox, -1, false);

        return banner;
    }

    void MultiRedditBanner::OnMultiUpdated(const api::MultiRedditRef& multi)
    {
        m_NameLabel->set_text(multi->GetDisplayName());

        for (Gtk::Widget* child = m_DescBox->get_last_child(); child != nullptr;)
        {
            Gtk::Widget* nextChild = child->get_prev_sibling();
            m_DescBox->remove(*child);
            child = nextChild;
        }

        util::HtmlParser htmlParser;
        htmlParser.ParseHtml(multi->GetDescriptionHtml(), m_DescBox, -1, false);
    }
}