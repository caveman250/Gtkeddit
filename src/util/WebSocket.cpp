#include "WebSocket.h"

#include <utility>
#include <websocketpp/config/asio_client.hpp>
#include <websocketpp/client.hpp>

#include <iostream>

typedef std::shared_ptr<boost::asio::ssl::context> context_ptr;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;
typedef websocketpp::config::asio_client::message_type::ptr message_ptr;

namespace util
{
    void on_message(WebSocket* websocket, const websocketpp::connection_hdl&, const message_ptr& msg)
    {
        websocket->OnMessage(msg->get_payload());
    }

    static context_ptr on_tls_init()
    {
        context_ptr ctx = std::make_shared<boost::asio::ssl::context>(boost::asio::ssl::context::sslv23);
        ctx->set_options(boost::asio::ssl::context::default_workarounds |
                         boost::asio::ssl::context::no_sslv2 |
                         boost::asio::ssl::context::no_sslv3 |
                         boost::asio::ssl::context::single_dh_use);
        return ctx;
    }

    WebSocket::WebSocket(const std::string& url)
        : m_Client(new client())
        , m_OnMessage(nullptr)
    {
        m_Client->set_access_channels(websocketpp::log::alevel::all);
        m_Client->clear_access_channels(websocketpp::log::alevel::frame_payload);

        m_Client->init_asio();
        m_Client->set_tls_init_handler(bind(&on_tls_init));
        m_Client->set_message_handler(bind(&on_message, this, ::_1, ::_2));

        websocketpp::lib::error_code ec;
        client::connection_ptr con = m_Client->get_connection(url, ec);
        if (ec)
        {
            g_warning("websocket connection error: %s", ec.message().c_str());
            return;
        }

        m_Client->connect(con);

        m_Thread = new std::thread([this]()
                                   {
                                       m_Client->run();
                                   });
    }

    void WebSocket::OnMessage(const std::string& msg)
    {
        if (m_OnMessage)
        {
            m_OnMessage(msg);
        }
    }

    void WebSocket::Stop()
    {
        m_Client->stop();
        //thread will end momentarily.
        m_Thread->detach();
    }

    void WebSocket::SetMessageCallback(std::function<void(std::string)> onMessage)
    {
        m_OnMessage = std::move(onMessage);
    }
}