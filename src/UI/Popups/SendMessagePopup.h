#include "Popup.h"
#include "UI/ui_types_fwd.h"
#include "API/api_fwd.h"

namespace util
{
    class ThreadWorker;
    class WebSocket;
}

namespace ui
{
    class SendMessagePopup : public Popup
    {
    public:
        SendMessagePopup();
        SendMessagePopup(const std::string& username, const std::string& title);

        void Reload() override {}
        void Cleanup() override {}
        virtual SortType GetSortType() const override { return SortType::None; }
        virtual UISettings GetUISettings() const override { return UISettings(); }

    private:
        void OnPopupChoiceSelected(GtkResponseType response) override;
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;

        void CheckValid();

        void TrySendMessage();
        void ShowErrorMessage(const std::string& errorMsg);

        Gtk::Entry* m_RecipientEntry = nullptr;
        Gtk::Entry* m_TitleEntry = nullptr;
        Gtk::TextView* m_TextView = nullptr;

        std::string m_DefaultRecipient;
        std::string m_DefaultTitle;
    };
}
