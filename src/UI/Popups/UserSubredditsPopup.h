#pragma once

#include "UI/Popups/Popup.h"

namespace ui
{
    class UserSubredditsPopup : public Popup
    {
    public:
        explicit UserSubredditsPopup(std::function<void(const SearchResultWidget*)> onClickOverride = nullptr, bool createAsPage = false);
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::None; }
        virtual UISettings GetUISettings() const override;
    private:
        virtual void OnPopupChoiceSelected(GtkResponseType ) override { /* ignore */}
        ui::UserSubredditsContentProviderRef m_ContentProvider;
        ui::RedditContentListBoxViewRef m_ListView;
        std::function<void(const SearchResultWidget*)> m_OnClickOverride;
    };
}
