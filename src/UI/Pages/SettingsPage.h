#pragma once

#include "Page.h"

namespace ui
{
    class SettingsPage : public Page
    {
    public:
        SettingsPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override;
        virtual UISettings GetUISettings() const override;

    private:
        Gtk::Box* m_Box = nullptr;
        Gtk::Box* m_ListsBox = nullptr;
        void EnsureCategoryExists(const std::string& category);
        void AddWidgetCommon(const std::string& name, const std::string& description, const std::string& category, Gtk::Widget* widget);
        void AddToggleSetting(const std::string& name, const std::string& description, const std::string& category, const std::string& setting_name, bool defaultVal, const std::function<void()>& onChanged = nullptr);
        void AddIntSetting(const std::string& name, const std::string& description, const std::string& category, const std::string& setting_name, int defaultVal, const std::function<void()>& onChanged = nullptr);

        void SetupPage();
        void CreateTitleWidget(const std::string& category);

        std::vector<std::string> m_Categories;

        struct SettingsCategory
        {
            std::string m_Name;
            std::vector<GtkWidget*> m_SettingsWidgets;
        };
        std::vector<SettingsCategory> m_SettingsWidgets;
        std::map<std::string, Gtk::ListBox*> m_CategoryBoxes;

        int m_RoundedCornersSettingChangedHandler;
    };
}
