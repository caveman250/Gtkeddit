#include "MainFeedPage.h"
#include "AppSettings.h"
#include "util/Signals.h"
#include "UI/ListViews/RedditContentListView.h"
#include "UI/ContentProviders/MainFeedContentProvider.h"
#include "UI/ContentProviders/PopularContentProvider.h"
#include "UI/ContentProviders/AllContentProvider.h"
#include "UI/Widgets/HeaderBar.h"

namespace ui
{
    MainFeedPage::MainFeedPage()
    : Page(PageType::MainFeedPage)
    , m_HomeListView(std::make_shared<RedditContentListView>(std::make_shared<MainFeedContentProvider>()))
    , m_PopularListView(std::make_shared<RedditContentListView>(std::make_shared<PopularContentProvider>()))
    , m_AllListView(std::make_shared<RedditContentListView>(std::make_shared<AllContentProvider>()))
    , m_LoginStatusChangedHandler(util::s_InvalidSignalHandlerID)
    {
    }

    MainFeedPage::~MainFeedPage()
    {
        if (m_LoginStatusChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->LoginStatusChanged.RemoveHandler(m_LoginStatusChangedHandler);
        }
    }

    void MainFeedPage::Cleanup()
    {
    }

    void MainFeedPage::Reload()
    {
        m_HomeListView->ClearContent();
        m_HomeListView->LoadContentAsync();

        m_PopularListView->ClearContent();
        m_PopularListView->LoadContentAsync();

        m_AllListView->ClearContent();
        m_AllListView->LoadContentAsync();
    }

    Gtk::Box*  MainFeedPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/main_feed.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("MainFeedBox");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        m_Stack = (AdwViewStack*)builder->get_widget<Gtk::Widget>("MainFeedViewStack")->gobj();
        m_ViewSwitcherBar = (AdwViewSwitcherBar *)builder->get_widget<Gtk::Widget>("switcher_bar")->gobj();

        Gtk::Viewport* postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox = builder->get_widget<Gtk::Box>("PostsBox");
        m_HomeListView->CreateUI(postsBox, postsViewport);
        m_HomeListView->LoadContentAsync();

        Gtk::Viewport* popularViewport = builder->get_widget<Gtk::Viewport>("PopularViewport");
        Gtk::Box* popularBox = builder->get_widget<Gtk::Box>("PopularBox");
        m_PopularListView->CreateUI(popularBox, popularViewport);
        m_PopularListView->LoadContentAsync();

        Gtk::Viewport* allViewport = builder->get_widget<Gtk::Viewport>("AllViewport");
        Gtk::Box* allBox = builder->get_widget<Gtk::Box>("AllBox");
        m_AllListView->CreateUI(allBox, allViewport);
        m_AllListView->LoadContentAsync();

        m_LoginStatusChangedHandler = util::Signals::Get()->LoginStatusChanged.AddHandler([this](bool)
        {
            Reload();
        });

        return box;
    }

    UISettings MainFeedPage::GetUISettings() const
    {
        return UISettings
        {
            false,
            true,
            false,
            true,
            true,
            true,
            false,
            false
        };
    }

    void MainFeedPage::OnHeaderBarCreated()
    {
        AdwViewSwitcherTitle* viewSwitcher = GetHeaderBar()->AddViewSwitcher(m_Stack);
        g_object_bind_property(viewSwitcher, "title-visible", m_ViewSwitcherBar, "reveal", GBindingFlags::G_BINDING_SYNC_CREATE);
    }
}