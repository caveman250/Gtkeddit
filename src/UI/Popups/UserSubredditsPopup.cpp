#include "UserSubredditsPopup.h"
#include "AppSettings.h"
#include "Application.h"
#include "UI/Widgets/SearchResultWidget.h"
#include "UI/Pages/SubredditPage.h"
#include "UI/ContentProviders/UserSubredditsContentProvider.h"
#include "UI/ListViews/RedditContentListBoxView.h"

namespace ui
{
    UserSubredditsPopup::UserSubredditsPopup(std::function<void(const SearchResultWidget*)> onClickOverride, bool createAsPage)
        : Popup(PageType::UserSubredditsPage, createAsPage)
        , m_ContentProvider(std::make_shared<UserSubredditsContentProvider>())
        , m_ListView(std::make_shared<ui::RedditContentListBoxView>(m_ContentProvider))
        , m_OnClickOverride(onClickOverride)
    {

    }

    Gtk::Box*  UserSubredditsPopup::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/posts_view.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("PostsView");

        Gtk::Viewport* postsViewport;
        postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox;
        postsBox = builder->get_widget<Gtk::Box>("PostsBox");
        m_ListView->CreateUI(postsBox, postsViewport);
        m_ListView->GetListBox()->set_sort_func([](Gtk::ListBoxRow* a, Gtk::ListBoxRow* b)
        {
            return a->get_name() > b->get_name();
        });
        m_ContentProvider->SetItemClickHandler([this](const SearchResultWidget* searchResult)
        {
            if (m_OnClickOverride)
            {
                m_OnClickOverride(searchResult);
            }
            else
            {
                ui::SubredditPageRef subView = std::make_shared<ui::SubredditPage>(searchResult->GetSubreddit());
                Application::Get()->AddPage(subView);
            }
        });
        m_ListView->LoadContentAsync();

        CreatePopup(parent, box, "Subreddits");

        return box;
    }

    void UserSubredditsPopup::Cleanup()
    {
    }

    void UserSubredditsPopup::Reload()
    {
        m_ListView->ClearContent();
        m_ContentProvider->SetReloadRequired(true);
        m_ListView->LoadContentAsync();
    }

    UISettings UserSubredditsPopup::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            true,
            false,
            true,
            false,
            false
        };
    }
}
