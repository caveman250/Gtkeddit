#include "pch.h"
#include "Comment.h"
#include "util/HtmlParser.h"
#include "RedditAPI.h"

namespace api
{
    Comment::Comment(Json::Value jsonData, const std::string& kind)
        : m_UpVoted(false)
        , m_DownVoted(false)
    {
        m_UserName = jsonData["author"].asString();
        m_Text = jsonData["body_html"].asString();
        m_Score = jsonData["score"].asInt();
        m_ID = jsonData["id"].asString();
        m_LinkID = jsonData["link_id"].asString();
        m_PermaLink = jsonData["permalink"].asString();
        m_Subreddit = jsonData["subreddit"].asString();
        m_IsOriginalPoster = jsonData["is_submitter"].asBool();
        m_IsModerator = jsonData["distinguished"].asString() == "moderator"; //TODO; handle other types?
        m_Timestamp = jsonData["created_utc"].asUInt64();
        m_Kind = kind;

        std::string likes = jsonData["likes"].asString();
        if (!likes.empty())
        {
            if (likes == "true")
            {
                m_UpVoted = true;
            }
            else
            {
                m_DownVoted = true;
            }
        }

        m_Bookmarked = jsonData["saved"].asBool();

        Json::Value replies = jsonData.get("replies", Json::Value());
        ParseChildComments( replies);

        std::ostringstream oss;
        oss << m_Kind << "_" << m_ID;
        m_FullID = oss.str();
    }

    Comment::~Comment()
    {
        for (CommentRef& child : m_Children)
        {
            child.reset();
        }

        m_Children.clear();
    }

    bool Comment::IsValid()
    {
        return !m_UserName.empty() || IsPlaceholder();
    }

    void Comment::AddChild(const CommentRef& child)
    {
        m_Children.push_back(child);
    }

    void Comment::ParseChildComments(Json::Value replies)
    {
        if (!replies.isNull() && replies.isObject())
        {
            Json::Value repliesData = replies["data"];
            Json::Value repliesChildren = repliesData["children"];

            for (Json::Value& value : repliesChildren)
            {
                Json::Value commentData = value["data"];

                CommentRef childComment = std::make_shared<Comment>(commentData,
                                                    value["kind"].asString());
                AddChild(childComment);
            }
        }
    }

    void Comment::UpVote()
    {
        if (m_DownVoted)
        {
            RemoveDownVote();
        }

        m_UpVoted = true;
        m_Score++;

        api::RedditAPI::Get()->Vote(m_FullID, 1);
    }

    void Comment::RemoveUpVote()
    {
        m_UpVoted = false;
        m_Score--;
        api::RedditAPI::Get()->Vote(m_FullID, 0);
    }

    void Comment::DownVote()
    {
        if (m_UpVoted)
        {
            RemoveUpVote();
        }

        m_DownVoted = true;
        m_Score--;

        api::RedditAPI::Get()->Vote(m_FullID, -1);
    }

    void Comment::RemoveDownVote()
    {
        m_DownVoted = false;
        m_Score++;
        api::RedditAPI::Get()->Vote(m_FullID, 0);
    }

    void Comment::Bookmark()
    {
        api::RedditAPI::Get()->Save(m_FullID);
        m_Bookmarked = true;
    }

    void Comment::RemoveBookmark()
    {
        api::RedditAPI::Get()->UnSave(m_FullID);
        m_Bookmarked = false;
    }
}
