#pragma once

struct MHD_Daemon;
namespace util
{
    class HttpServer
    {
    public:
        HttpServer();
        void Run(int port);
        void Stop();

        void SetOnArgCallback(std::function<void(std::string, std::string)> cb);
        void OnArgRecieved(std::string key, std::string value);

        struct RunServerArgs
        {
            const char* m_Response;
            HttpServer* m_Server;
        };

        struct IterateArgumentsArgs
        {
            HttpServer* m_Server;
        };
    private:

        RunServerArgs* m_RunServerArgs;
        MHD_Daemon* m_MHDDaemon;
        std::function<void(std::string, std::string)> m_OnArgumentCallback;
    };
}
